package com.qb.xrealsys.ifafu.score.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.qb.xrealsys.ifafu.R;
import com.qb.xrealsys.ifafu.score.delegate.OpenScoreCalcDetailDelegate;
import com.qb.xrealsys.ifafu.score.model.EducationScoreCalcDetail;
import com.qb.xrealsys.ifafu.score.model.Score;

import java.util.Locale;

public class EducationScoreDetailDialog extends Dialog implements View.OnClickListener {

    private TextView calcDetailView;

    private OpenScoreCalcDetailDelegate delegate;

    public EducationScoreDetailDialog(@NonNull Context context, OpenScoreCalcDetailDelegate delegate) {
        super(context, R.style.styleIOSDialog);
        setContentView(R.layout.dialog_education_score_detail);

        findViewById(R.id.closeBtn).setOnClickListener(this);
        findViewById(R.id.queryBtn).setOnClickListener(this);

        calcDetailView = findViewById(R.id.calcDetailDisplay);
        this.delegate  = delegate;
    }

    public void show(EducationScoreCalcDetail calcDetail) {
        if (calcDetail == null || calcDetail.getAnswer() == 0) {
            return;
        }

        StringBuilder display = new StringBuilder();

        /* *
         *  排除课程显示
         */
        display.append("排除课程:[");

        for (String courseName: calcDetail.getElectiveCourses()) {
            display.append(courseName);
            display.append("(任意选修课)、");
        }

        for (String courseName: calcDetail.getPECourses()) {
            display.append(courseName);
            display.append("(体育课)、");
        }

        for (String courseName: calcDetail.getEnglishFilterCourses()) {
            display.append(courseName);
            display.append("(英语跳级)、");
        }

        for (String courseName: calcDetail.getAccidentCourses()) {
            display.append(courseName);
            display.append("(重修/缓考)、");
        }

        for (String courseName: calcDetail.getCustomFilterCourses()) {
            display.append(courseName);
            display.append("(自定义补/免修)、");
        }
        display.deleteCharAt(display.length() - 1);
        display.append("]\n");
        /* * * * * * * */

        /* *
         *  计算过程显示
         */
        display.append(String.format(
                Locale.getDefault(),
                "共有%d门课程纳入计算，其中不及格%d门\n",
                calcDetail.getActualCalcCourses().size(),
                calcDetail.getNoPassCourseCount()));

        display.append("加权总分为");
        for (Score score: calcDetail.getActualCalcCourses()) {
            display.append(String.format(
                    Locale.getDefault(),
                    "%.2f x %.2f + ",
                    score.getScore(),
                    score.getStudyScore()));
        }
        display.delete(display.length() - 3, display.length() - 1);
        display.append(String.format(Locale.getDefault(), " = %.2f\n", calcDetail.getTotalScore()));

        display.append("总学分为");
        for (Score score: calcDetail.getActualCalcCourses()) {
            display.append(String.format(
                    Locale.getDefault(),
                    "%.2f + ",
                    score.getStudyScore()));
        }
        display.delete(display.length() - 3, display.length() - 1);
        display.append(String.format(Locale.getDefault(), " = %.2f\n", calcDetail.getTotalStudyScore()));

        display.append(String.format(
                Locale.getDefault(),
                "则 %.2f / %.2f = %.2f分\n",
                calcDetail.getTotalScore(),
                calcDetail.getTotalStudyScore(),
                calcDetail.getTotalScore() / calcDetail.getTotalStudyScore()));

        display.append(String.format(
                Locale.getDefault(),
                "减去挂科扣分 %.2f分，最终为%.2f分",
                calcDetail.getMinus(),
                calcDetail.getAnswer()));
        /* * * * * * * */

        calcDetailView.setText(display.toString());
        super.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.closeBtn:
                this.cancel();
                break;
            case R.id.queryBtn:
                delegate.informOpenScoreCalcDetail();
                break;
        }
    }
}
