package com.qb.xrealsys.ifafu.backend.model;

import org.json.JSONException;
import org.json.JSONObject;
import org.jetbrains.annotations.NotNull;

public class AppInform {

    private int     id;

    private String  title;

    private String  content;

    private String  publishAt;

    private String  endAt;

    public AppInform(@NotNull JSONObject object) {

        try {
            setId(object.getInt("id"));
            setTitle(object.getString("title"));
            setContent(object.getString("content"));
            setPublishAt(object.getString("updated_at"));
            setEndAt(object.getString("end_at"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPublishAt() {
        return publishAt;
    }

    public void setPublishAt(String publishAt) {
        this.publishAt = publishAt;
    }

    public String getEndAt() {
        return endAt;
    }

    public void setEndAt(String endAt) {
        this.endAt = endAt;
    }
}
