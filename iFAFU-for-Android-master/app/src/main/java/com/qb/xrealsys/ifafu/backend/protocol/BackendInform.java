package com.qb.xrealsys.ifafu.backend.protocol;

import com.qb.xrealsys.ifafu.backend.model.LatestAppInform;

import org.json.JSONObject;

import java.util.Locale;

public class BackendInform extends BackendAccess {

    private static String latestUrl = "/public/app/inform/android?t=%d";

    public static LatestAppInform getLatestInform(String host, long t) {
        String url = host + String.format(Locale.getDefault(), latestUrl, t);

        JSONObject object = backendGet(url);
        if (object == null) {
            return null;
        }

        return new LatestAppInform(object);
    }
}
