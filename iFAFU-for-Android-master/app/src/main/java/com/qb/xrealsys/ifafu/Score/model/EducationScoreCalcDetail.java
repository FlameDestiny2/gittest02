package com.qb.xrealsys.ifafu.score.model;

import java.util.ArrayList;
import java.util.List;

public class EducationScoreCalcDetail {

    private List<String> electiveCourses;

    private List<String> PECourses;

    private List<String> customFilterCourses;

    private List<String> accidentCourses;

    private List<String> englishFilterCourses;

    private List<Score>  actualCalcCourses;

    private float        minus;

    private float        totalScore;

    private float        totalStudyScore;

    private float        answer;

    private int          noPassCourseCount;

    public EducationScoreCalcDetail() {
        this.electiveCourses = new ArrayList<>();
        this.PECourses       = new ArrayList<>();
        this.customFilterCourses = new ArrayList<>();
        this.accidentCourses     = new ArrayList<>();
        this.englishFilterCourses = new ArrayList<>();
        this.actualCalcCourses    = new ArrayList<>();
        this.minus              = 0;
        this.totalScore         = 0;
        this.totalStudyScore    = 0;
        this.noPassCourseCount  = 0;
    }

    public List<String> getElectiveCourses() {
        return electiveCourses;
    }

    public List<String> getPECourses() {
        return PECourses;
    }

    public List<String> getCustomFilterCourses() {
        return customFilterCourses;
    }

    public List<String> getAccidentCourses() {
        return accidentCourses;
    }

    public List<String> getEnglishFilterCourses() {
        return englishFilterCourses;
    }

    public List<Score> getActualCalcCourses() {
        return actualCalcCourses;
    }

    public float getMinus() {
        return minus;
    }

    public float getTotalScore() {
        return totalScore;
    }

    public float getTotalStudyScore() {
        return totalStudyScore;
    }

    public void setMinus(float minus) {
        this.minus = minus;
    }

    public void setTotalScore(float totalScore) {
        this.totalScore = totalScore;
    }

    public void setTotalStudyScore(float totalStudyScore) {
        this.totalStudyScore = totalStudyScore;
    }

    public float getAnswer() {
        return answer;
    }

    public void setAnswer(float answer) {
        this.answer = answer;
    }

    public int getNoPassCourseCount() {
        return noPassCourseCount;
    }

    public void noPassCoursePlus() {
        this.noPassCourseCount++;
    }

    public void updateCustomFilterCourses(List<String> newList) {
        customFilterCourses.clear();
        customFilterCourses.addAll(newList);
    }
}
