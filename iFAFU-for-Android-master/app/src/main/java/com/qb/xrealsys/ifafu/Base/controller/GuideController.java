package com.qb.xrealsys.ifafu.base.controller;

import com.qb.xrealsys.ifafu.db.GuideRecord;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

public class GuideController {

    private Map<String, Boolean> guideMap;

    public GuideController() {
        guideMap = new HashMap<>();
        RealmResults<GuideRecord> records = Realm.getDefaultInstance().where(GuideRecord.class).findAll();
        for (GuideRecord guideRecord: records) {
            guideMap.put(guideRecord.getGuideName(), true);
        }
    }

    public boolean loadScoreActivityGuide() {
        return getOrDefault("scoreActivity");
    }

    public boolean loadSyllabusActivityGuide() {
        return getOrDefault("syllabusActivity");
    }

    private boolean getOrDefault(final String key) {
        if (guideMap.containsKey(key)) {
            return false;
        } else {
            Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm r) {
                    GuideRecord guideRecord = new GuideRecord();
                    guideRecord.setGuideName(key);
                    r.insertOrUpdate(guideRecord);
//                    r.commitTransaction();
                }
            });
            guideMap.put(key, true);
            return true;
        }
    }
}
