package com.qb.xrealsys.ifafu.backend.delegate;

import com.qb.xrealsys.ifafu.backend.model.LatestAppInform;

public interface ReadBackendInformCallback {

    void readBackendInform(LatestAppInform informList);
}
