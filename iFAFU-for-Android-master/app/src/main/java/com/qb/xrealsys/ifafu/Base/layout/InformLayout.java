package com.qb.xrealsys.ifafu.base.layout;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.qb.xrealsys.ifafu.R;

public class InformLayout extends FrameLayout {
    private TextView title;
    private TextView content;
    private ImageView redDot;

    public InformLayout(@NonNull Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_inform_item,this);
        init();
    }

    private void init() {
        title = (TextView) findViewById(R.id.news_title);
        content = (TextView) findViewById(R.id.news_content);
        redDot = (ImageView) findViewById(R.id.news_redDot);
    }

    public void setRedDotGone() {
        redDot.setVisibility(View.GONE);
    }

    public void setTitle(String s) {
        title.setText(s);
    }

    public String getTitle() {
        return title.getText().toString();
    }

    public void setContent(String s) {
        content.setText(s);
    }

    public String getContent() {
        return content.getText().toString();
    }


}
