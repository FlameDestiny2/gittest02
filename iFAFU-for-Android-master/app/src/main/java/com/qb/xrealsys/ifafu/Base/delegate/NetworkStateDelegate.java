package com.qb.xrealsys.ifafu.base.delegate;

public interface NetworkStateDelegate {

    boolean networkCheckCallback(boolean isConnected);
}
