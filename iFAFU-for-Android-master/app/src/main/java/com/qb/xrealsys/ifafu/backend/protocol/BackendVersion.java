package com.qb.xrealsys.ifafu.backend.protocol;

import com.qb.xrealsys.ifafu.backend.model.AppVersion;

import org.json.JSONObject;

public class BackendVersion extends BackendAccess {

    private static String versionUrl = "/public/app/version/android";

    public static AppVersion getLatestVersion(String host) {
        String      url    = host + versionUrl;
        JSONObject  object = backendGet(url);
        if (object == null) {
            return null;
        }

        return new AppVersion(object);
    }
}
