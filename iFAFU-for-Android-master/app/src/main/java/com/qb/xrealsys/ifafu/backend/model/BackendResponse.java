package com.qb.xrealsys.ifafu.backend.model;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

public class BackendResponse {

    private int     status;

    private String  error;

    private boolean complete;

    public BackendResponse(@NotNull JSONObject object) {
        complete = false;
        if (object.has("status") && object.has("error")) {
            try {
                setStatus(object.getInt("status"));
                setError(object.getString("error"));
                complete = true;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }
}
