package com.qb.xrealsys.ifafu.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class BackendCache extends RealmObject {

    @PrimaryKey
    private String key;

    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
