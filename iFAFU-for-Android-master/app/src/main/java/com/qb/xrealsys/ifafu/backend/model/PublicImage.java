package com.qb.xrealsys.ifafu.backend.model;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

public class PublicImage extends BackendResponse {

    private String  staticUrl;

    private int     t;

    public PublicImage(@NotNull JSONObject object) {
        super(object);

        if (getStatus() == 0) {
            setComplete(false);
        }
        if (getStatus() == 0 && object.has("static_url") && object.has("t")) {
            try {
                setStaticUrl(object.getString("static_url"));
                setT(object.getInt("t"));
                setComplete(true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getStaticUrl() {
        return staticUrl;
    }

    public void setStaticUrl(String staticUrl) {
        this.staticUrl = staticUrl;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }
}
