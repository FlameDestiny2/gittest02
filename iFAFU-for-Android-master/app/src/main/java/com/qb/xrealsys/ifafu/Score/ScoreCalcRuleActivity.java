package com.qb.xrealsys.ifafu.score;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.qb.xrealsys.ifafu.MainApplication;
import com.qb.xrealsys.ifafu.R;
import com.qb.xrealsys.ifafu.base.controller.TitleBarController;
import com.qb.xrealsys.ifafu.base.delegate.TitleBarButtonOnClickedDelegate;

public class ScoreCalcRuleActivity extends AppCompatActivity implements TitleBarButtonOnClickedDelegate {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_copying);

        TitleBarController titleBarController  = new TitleBarController(this);
        titleBarController
                .setBigPageTitle("智育分计算规则")
                .setHeadBack()
                .setOnClickedListener(this);

        String content = ((MainApplication) getApplication()).getBackendInterface().getScoreCalcRule();

        TextView textView = findViewById(R.id.copyingView);
        if (content != null) {
            textView.setText(content);
        }
    }

    @Override
    public void titleBarOnClicked(int id) {
        finish();
    }

    @Override
    public void titleBarOnLongClicked(int id) {

    }
}
