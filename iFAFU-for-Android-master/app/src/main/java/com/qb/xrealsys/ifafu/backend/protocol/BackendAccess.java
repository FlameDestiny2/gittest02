package com.qb.xrealsys.ifafu.backend.protocol;

import com.qb.xrealsys.ifafu.tool.HttpHelper;
import com.qb.xrealsys.ifafu.tool.HttpResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

class BackendAccess {

    static JSONObject backendGet(String url) {
        return _resolveResponse(_backendGet(url, 0));
    }

    private static HttpResponse _backendGet(String url, int retry) {
        if (retry > 2) {
            return null;
        }

        try {
            HttpHelper request = new HttpHelper(url);
            return request.Get();
        } catch (IOException e) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            return _backendGet(url, retry + 1);
        }
    }

    static JSONObject backendPost(String url, Map<String, String> postData) {
        return _resolveResponse(_backendPost(url, postData, 0));
    }

    private static HttpResponse _backendPost(String url, Map<String, String> postData, int retry) {
        if (retry > 2) {
            return null;
        }

        try {
            HttpHelper request = new HttpHelper(url);
            return request.Post(postData);
        } catch (IOException e) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            return _backendPost(url, postData, retry + 1);
        }
    }

    private static JSONObject _resolveResponse(HttpResponse response) {
        if (response == null || response.getStatus() != 200) {
            return null;
        }

        try {
            return new JSONObject(response.getResponse());
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
