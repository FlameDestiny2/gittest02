package com.qb.xrealsys.ifafu.backend.protocol;

import com.qb.xrealsys.ifafu.backend.model.BackendResponse;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class BackendReport extends BackendAccess {

    private static String registerUrl   = "/public/report/%s/register";

    private static String userInfoUrl   = "/public/report/%s/user/info";

    private static String stateUrl      = "/public/report/%s/state/%s";

    public static BackendResponse register(String host, String deviceNo, String deviceContent) {
        String url = host + String.format(Locale.getDefault(), registerUrl, deviceNo);

        Map<String, String> postData = new HashMap<>();
        postData.put("deviceType", "android");
        postData.put("deviceContent", deviceContent);

        JSONObject object = backendPost(url, postData);
        if (object == null) {
            return null;
        }

        return new BackendResponse(object);
    }

    public static BackendResponse reportUser(String host, String deviceNo, String stuNo, String stuClass, String stuCollege, String stuMajor) {
        String url = host + String.format(Locale.getDefault(), userInfoUrl, deviceNo);

        Map<String, String> postData = new HashMap<>();
        postData.put("stuNo", stuNo);
        postData.put("stuClass", stuClass);
        postData.put("stuCollege", stuCollege);
        postData.put("stuMajor", stuMajor);

        JSONObject object = backendPost(url, postData);
        if (object == null) {
            return null;
        }

        return new BackendResponse(object);
    }

    public static BackendResponse reportState(String host, String deviceNo, String option) {
        String url = host + String.format(Locale.getDefault(), stateUrl, deviceNo, option);

        JSONObject object = backendGet(url);
        if (object == null) {
            return null;
        }

        return new BackendResponse(object);
    }
}
