package com.qb.xrealsys.ifafu.backend.controller;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.qb.xrealsys.ifafu.backend.model.PublicImage;
import com.qb.xrealsys.ifafu.backend.model.PublicText;
import com.qb.xrealsys.ifafu.backend.protocol.BackendResource;
import com.qb.xrealsys.ifafu.db.BackendCache;
import com.qb.xrealsys.ifafu.tool.FileHelper;
import com.qb.xrealsys.ifafu.tool.HttpHelper;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class ResourceController extends BackendController {

    private static String imgPath = "/img/";

    private String firstWeek;

    private String scoreCalcRule;

    private Bitmap background;

    public ResourceController(String host) {
        super(host);
    }

    public Bitmap getBackground() {
        if (background == null) {
            background = readBitmapFromFile("background");

            if (background == null) {
                setBackendCache("background_t", String.valueOf(0));
            }
        }

        return background;
    }

    public String getFirstWeek() {
        if (firstWeek == null) {
            firstWeek = getBackendCacheText("firstWeek");
        }

        return firstWeek;
    }

    public String getScoreCalcRule() {
        if (scoreCalcRule == null) {
            scoreCalcRule = getBackendCacheText("scoreCalcRule");
        }

        return scoreCalcRule;
    }

    public void loadScoreCalcRule() {
        BackendCache backendCache = getBackendCache("scoreCalcRule_t");

        int t = 0;
        if (backendCache != null) {
            t = Integer.parseInt(backendCache.getValue());
        }

        PublicText publicText = BackendResource.getPublicText(host, "scoreCalcRule", t);
        if (requestIsFailed(publicText)) {
            scoreCalcRule = getBackendCacheText("scoreCalcRule");
        } else {
            if (publicText.getStatus() == 0) {
                scoreCalcRule = publicText.getContent();
                setBackendCache("scoreCalcRule", scoreCalcRule);
            } else {
                scoreCalcRule = getBackendCacheText("scoreCalcRule");
            }
        }
    }


    public void loadFirstWeek() {
        BackendCache backendCache = getBackendCache("firstWeek_t");

        int t = 0;
        if (backendCache != null) {
            t = Integer.parseInt(backendCache.getValue());
        }

        PublicText publicText = BackendResource.getPublicText(host, "firstWeek", t);
        if (requestIsFailed(publicText)) {
            firstWeek = getBackendCacheText("firstWeek");
        } else {
            if (publicText.getStatus() == 0) {
                firstWeek = publicText.getContent();
                setBackendCache("firstWeek", firstWeek);
            } else {
                firstWeek = getBackendCacheText("firstWeek");
            }
        }
    }

    public void downloadBackground() {
        BackendCache backendCache = getBackendCache("background_t");

        int t = 0;
        if (backendCache != null) {
            t = Integer.parseInt(backendCache.getValue());
        }

        PublicImage publicImage = BackendResource.getPublicImage(host, "background", t);
        if (requestIsFailed(publicImage)) {
            background = readBitmapFromFile("background");
        } else {
            if (publicImage.getStatus() == 0) {
                background = downloadFromServer(publicImage.getStaticUrl());
                writeBitmapIntoFile("background", background);
                setBackendCache("background_t", String.valueOf(publicImage.getT()));
            } else {
                background = readBitmapFromFile("background");
            }
        }
    }

    private boolean writeBitmapIntoFile(String key, Bitmap bitmap) {
        if (!FileHelper.isExistFile(imgPath)) {
            if (!FileHelper.mkDir(imgPath)) {
                return false;
            }
        }

        File file = FileHelper.getFilePath(imgPath + key);

        try {
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    return false;
                }
            }

            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private Bitmap readBitmapFromFile(String key) {
        if (!FileHelper.isExistFile(imgPath)) {
            if (!FileHelper.mkDir(imgPath)) {
                return null;
            }
        }

        File file = FileHelper.getFilePath(imgPath + key);
        if (!file.exists() || !file.isFile()) {
            return null;
        }

        FileInputStream fis;
        try {
            fis = new FileInputStream(file);
            Bitmap bitmap = BitmapFactory.decodeStream(fis);
            fis.close();
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Bitmap downloadFromServer(String url) {
        try {
            HttpHelper  request = new HttpHelper(url);
            return request.GetHttpGragh();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
