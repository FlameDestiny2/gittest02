package com.qb.xrealsys.ifafu.backend.model;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LatestAppInform extends BackendResponse {

    private long latest;

    private List<AppInform> informList;

    public LatestAppInform(@NotNull JSONObject object) {
        super(object);
        informList = new ArrayList<>();
        latest     = 0;

        setComplete(false);
        if (getStatus() == 0) {
            try {
                latest = object.getLong("latest");

                JSONArray array = new JSONArray(object.getString("list"));
                for (int i = 0; i < array.length(); i++) {
                    informList.add(new AppInform((JSONObject) array.get(i)));
                }

                setComplete(true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public long getLatest() {
        return latest;
    }

    public List<AppInform> getInformList() {
        return informList;
    }
}
