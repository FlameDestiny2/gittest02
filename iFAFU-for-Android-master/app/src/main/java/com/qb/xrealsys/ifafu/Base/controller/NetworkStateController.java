package com.qb.xrealsys.ifafu.base.controller;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import com.qb.xrealsys.ifafu.base.delegate.NetworkStateDelegate;
import com.qb.xrealsys.ifafu.base.web.WebInterface;
import com.qb.xrealsys.ifafu.tool.ConfigHelper;

import java.util.concurrent.ExecutorService;

public class NetworkStateController {

    private ConnectivityManager     connectivityManager;

    private ConfigHelper            configHelper;

    private ExecutorService         threadPool;

    private NetworkStateDelegate    callback;

    public NetworkStateController(Context context, ConfigHelper configHelper, ExecutorService threadPool) {
        this.connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        this.configHelper        = configHelper;
        this.threadPool          = threadPool;
    }

    public boolean isNetworkConnected() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return isNetworkConnectedNew();
        } else {
            return isNetworkConnectedOld();
        }
    }

    private boolean isNetworkConnectedOld() {
        NetworkInfo pLTEState   = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo pWIFIState  = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        Log.i("APPLICATION", "LTE STATE IS: " + pLTEState.isConnected());
        Log.i("APPLICATION", "WIFI STATE IS: " + pWIFIState.isConnected());
        return pLTEState.isConnected() || pWIFIState.isConnected();
    }

    private boolean isNetworkConnectedNew() {
        Network[] networks = connectivityManager.getAllNetworks();

        for (Network network: networks) {
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);

            if (networkInfo.isConnected()) {
                Log.i("APPLICATION", networkInfo.getTypeName() + " STATE IS: true");
                return true;
            }
        }

        return false;
    }

    public void detectZF(final boolean isOnline) {
        if (!isOnline) {
            callback.networkCheckCallback(false);
        } else {
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    callback.networkCheckCallback(WebInterface.detectZF(configHelper.getSystemValue("host")));
                }
            });
        }

    }

    public void setCallback(NetworkStateDelegate callback) {
        this.callback = callback;
    }
}
