package com.qb.xrealsys.ifafu.tool;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class FileHelper {

    private static String rootPath;

    public static void setHavePermissions(Context context, boolean isPermissions) {
        if (isPermissions) {
            rootPath = context.getExternalFilesDir("ifafu").getPath();
        } else {
            rootPath = context.getFilesDir().getPath();
        }
    }

    public static boolean isExistFile(String dir) {
        File file = getFilePath(dir);
        return file.exists();
    }

    public static boolean mkDir(String dir) {
        File file = getFilePath(dir);
        return file.mkdirs();
    }

    public static File getFilePath(String dir) {
        return new File(rootPath + dir);
    }
}
