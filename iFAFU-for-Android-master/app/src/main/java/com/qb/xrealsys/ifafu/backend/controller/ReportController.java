package com.qb.xrealsys.ifafu.backend.controller;

import android.content.Context;
import android.util.Log;

import com.qb.xrealsys.ifafu.backend.model.BackendResponse;
import com.qb.xrealsys.ifafu.backend.protocol.BackendReport;
import com.qb.xrealsys.ifafu.tool.ReadPhone;

public class ReportController extends BackendController {

    public ReportController(String host) {
        super(host);
    }

    public String register(Context context) {
        String deviceId = ReadPhone.readUUID(context);
        String content  = ReadPhone.readDeviceContent();

        BackendResponse response = BackendReport.register(host, deviceId, content);
        if (response != null && response.isComplete() && response.getStatus() == 0) {
            Log.i("BACKEND", "REGISTER_DEVICE_SUCCESS: " + deviceId);
        }

        return deviceId;
    }

    public void reportUser(String deviceId, String stuNo, String stuClass, String stuCollege, String stuMajor) {
        BackendReport.reportUser(host, deviceId, stuNo, stuClass, stuCollege, stuMajor);
    }

    public void reportState(String deviceId, String option) {
        BackendReport.reportState(host, deviceId, option);
    }
}
