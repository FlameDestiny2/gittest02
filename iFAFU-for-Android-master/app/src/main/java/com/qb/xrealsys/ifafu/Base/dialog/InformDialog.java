package com.qb.xrealsys.ifafu.base.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.qb.xrealsys.ifafu.R;
import com.qb.xrealsys.ifafu.backend.model.AppInform;
import com.qb.xrealsys.ifafu.backend.model.LatestAppInform;
import com.qb.xrealsys.ifafu.base.layout.InformLayout;

import java.util.List;


public class InformDialog extends Dialog implements View.OnClickListener {

    private boolean statu = false;
    private Context context;
    private View firstView;
    private View secondView;
    private LinearLayout newsLayout;

    public InformDialog(@NonNull Context context) {
        super(context, R.style.styleInformDialog);
        this.context = context;
    }

    public InformDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
    }

    protected InformDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
    }

    private ImageView backFirst;
    private ImageView backSecond;
    private ImageButton backToFirst;
    private ListView newsItem;
    private TextView newsCount;
    private List<AppInform> newsList;
    private ScrollView scrollView;

    private void setDialogSize() {
        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 0.6); // 高度设置为屏幕的0.8
        lp.height = (int) (d.heightPixels * 0.7);
        dialogWindow.setAttributes(lp);
    }

    private void initFirstPage() {
        firstView = LayoutInflater.from(context).inflate(R.layout.dialog_inform_first, null);
        newsCount = (TextView) firstView.findViewById(R.id.news_count);
        scrollView = (ScrollView) firstView.findViewById(R.id.news_list);
        newsLayout = (LinearLayout) firstView.findViewById(R.id.news_layout);
        backFirst = (ImageView) firstView.findViewById(R.id.news_back_first);
        backFirst.setOnClickListener(this);
        newsCount.setText(newsList.size() + "条新通知");
    }

    private void initSecondPage() {
        secondView = LayoutInflater.from(context).inflate(R.layout.dialog_inform_second, null);
        backToFirst = (ImageButton) secondView.findViewById(R.id.back_to_first);
        backToFirst.setOnClickListener(this);
        backSecond = (ImageView) secondView.findViewById(R.id.news_back_second);
        backSecond.setOnClickListener(this);
    }

    public void show(LatestAppInform informList) {

        this.newsList = informList.getInformList();
        setCanceledOnTouchOutside(false);

        if(newsList.size() > 1) {
            initFirstPage();
            initSecondPage();
            setContentView(firstView);
            for(int i = 0; i < newsList.size(); i++) {

                final InformLayout informLayout = new InformLayout(context);
                informLayout.setTitle(newsList.get(i).getTitle());
                informLayout.setContent(newsList.get(i).getContent());
                informLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        informLayout.setRedDotGone();
                        statu = true;
                        TextView t = (TextView) secondView.findViewById(R.id.item_title);
                        TextView c = (TextView) secondView.findViewById(R.id.item_content);
                        t.setText(informLayout.getTitle());
                        c.setText(informLayout.getContent());
                        setContentView(secondView);
                    }
                });
                newsLayout.addView(informLayout);
            }

        } else {
            initSecondPage();
            TextView t = (TextView) secondView.findViewById(R.id.item_title);
            TextView c = (TextView) secondView.findViewById(R.id.item_content);
            t.setText(newsList.get(0).getTitle());
            c.setText(newsList.get(0).getContent());
            backToFirst.setVisibility(View.GONE);
            setContentView(secondView);
        }
        setDialogSize();

        super.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.news_back_first: dismiss(); break;
            case R.id.news_back_second: dismiss(); break;
            case R.id.back_to_first: refreshNewsList(); statu = false; break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if(statu == true) {
                refreshNewsList();
                statu = false;
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void refreshNewsList() {
        setContentView(firstView);
    }
}
