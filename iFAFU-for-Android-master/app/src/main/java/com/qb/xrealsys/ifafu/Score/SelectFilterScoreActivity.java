package com.qb.xrealsys.ifafu.score;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.qb.xrealsys.ifafu.MainApplication;
import com.qb.xrealsys.ifafu.R;
import com.qb.xrealsys.ifafu.base.controller.TitleBarController;
import com.qb.xrealsys.ifafu.base.delegate.TitleBarButtonOnClickedDelegate;
import com.qb.xrealsys.ifafu.score.controller.ScoreAsyncController;
import com.qb.xrealsys.ifafu.score.model.Score;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectFilterScoreActivity extends AppCompatActivity implements
        TitleBarButtonOnClickedDelegate, View.OnClickListener {

    private ScoreAsyncController    scoreController;

    private SparseBooleanArray      checkMap;

    private List<Score>             scoreList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_filter_score);

        TitleBarController titleBarController  = new TitleBarController(this);
        titleBarController
                .setBigPageTitle("设置补修/免修课程")
                .setHeadBack()
                .setOnClickedListener(this);

        checkMap = new SparseBooleanArray();

        MainApplication mainApplication = (MainApplication) getApplication();
        scoreController = mainApplication.getScoreController();

        fillListView();
        findViewById(R.id.submitBtn).setOnClickListener(this);
    }

    private void fillListView() {
        ListView    courseListView  = findViewById(R.id.courseList);
        scoreList                   = scoreController.getData().getData();

        List<Map<String, Object>> adapterData = new ArrayList<>();
        for (Score score: scoreList) {
            Map<String, Object> map = new HashMap<>();
            map.put("text", score.getCourseName());
            adapterData.add(map);
        }

        SelectCourseAdapter simpleAdapter = new SelectCourseAdapter(
                this,
                adapterData,
                R.layout.gadget_item_select_course,
                new String[] {"text"},
                new int[] {R.id.checkItem});

        courseListView.setAdapter(simpleAdapter);
    }

    @Override
    public void titleBarOnClicked(int id) {
        finish();
    }

    @Override
    public void titleBarOnLongClicked(int id) {

    }

    @Override
    public void onClick(View view) {
        List<String> newList = new ArrayList<>();

        for (int i = 0; i < scoreList.size(); i++) {
            if (checkMap.get(i, false)) {
                newList.add(scoreList.get(i).getCourseName());
            }
        }

        scoreController.updateCustomFilterCourses(newList);

        finish();
    }

    private class SelectCourseAdapter extends SimpleAdapter {

        private Context mContext;

        SelectCourseAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);

            mContext = context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.gadget_item_select_course, null);
            }

            convertView.findViewById(R.id.checkItem).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    checkMap.put(position, !checkMap.get(position, false));
//                    Toast.makeText(SelectFilterScoreActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                }
            });

            CheckBox checkBox = convertView.findViewById(R.id.checkItem);
            checkBox.setChecked(checkMap.get(position, false));
            checkBox.setText(scoreList.get(position).getCourseName());

            return convertView;
        }
    }
}
