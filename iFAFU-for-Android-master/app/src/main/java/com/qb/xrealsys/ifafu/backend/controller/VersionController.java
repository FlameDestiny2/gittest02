package com.qb.xrealsys.ifafu.backend.controller;

import com.qb.xrealsys.ifafu.backend.model.AppVersion;
import com.qb.xrealsys.ifafu.backend.protocol.BackendVersion;

public class VersionController extends BackendController {

    private AppVersion appVersion;

    public VersionController(String host) {
        super(host);
    }

    public AppVersion getAppVersion() {
        return appVersion;
    }

    public void loadAppLatestVersion() {
        appVersion = BackendVersion.getLatestVersion(host);
    }
}
