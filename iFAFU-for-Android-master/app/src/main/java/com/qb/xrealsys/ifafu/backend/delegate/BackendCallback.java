package com.qb.xrealsys.ifafu.backend.delegate;

public interface BackendCallback {

    void loadFinished();

    void registerDeviceFinished(String deviceId);
}
