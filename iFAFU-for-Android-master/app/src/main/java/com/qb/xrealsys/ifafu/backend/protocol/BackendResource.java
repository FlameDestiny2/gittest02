package com.qb.xrealsys.ifafu.backend.protocol;

import com.qb.xrealsys.ifafu.backend.model.PublicImage;
import com.qb.xrealsys.ifafu.backend.model.PublicText;


import org.json.JSONObject;

import java.util.Locale;

public class BackendResource extends BackendAccess {

    private static String imgUrl    = "/public/img/%s?t=%d";

    private static String textUrl   = "/public/text/%s?t=%d";

    public static PublicText getPublicText(String host, String key, int t) {
        String      url         = host + String.format(Locale.getDefault(), textUrl, key, t);
        JSONObject  object      = backendGet(url);
        if (object == null) {
            return null;
        }

        return new PublicText(object);
    }

    public static PublicImage getPublicImage(String host, String key, int t) {
        String      url         = host + String.format(Locale.getDefault(), imgUrl, key, t);
        JSONObject  object      = backendGet(url);
        if (object == null) {
            return null;
        }

        return new PublicImage(object);
    }
}
