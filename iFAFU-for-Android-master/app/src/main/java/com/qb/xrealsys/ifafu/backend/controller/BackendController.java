package com.qb.xrealsys.ifafu.backend.controller;


import com.qb.xrealsys.ifafu.backend.model.BackendResponse;
import com.qb.xrealsys.ifafu.db.BackendCache;

import io.realm.Realm;

public class BackendController {

    protected String          host;

    BackendController(String host) {
        this.host       = host;
    }

    boolean requestIsFailed(BackendResponse response) {
        return response == null || !response.isComplete();
    }

    BackendCache getBackendCache(String key) {
        return Realm.getDefaultInstance()
                .where(BackendCache.class)
                .equalTo("key", key)
                .findFirst();
    }

    void setBackendCache(final String key, final String value) {
        Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {
                BackendCache backendCache = new BackendCache();
                backendCache.setKey(key);
                backendCache.setValue(value);
                realm.insertOrUpdate(backendCache);
//                realm.commitTransaction();
            }
        });
    }

    String getBackendCacheText(String key) {
        BackendCache backendCache = getBackendCache(key);
        if (backendCache == null) {
            return null;
        }

        return backendCache.getValue();
    }
}
