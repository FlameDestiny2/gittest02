package com.qb.xrealsys.ifafu.backend.controller;

import com.qb.xrealsys.ifafu.backend.model.LatestAppInform;
import com.qb.xrealsys.ifafu.backend.protocol.BackendInform;

public class InformController extends BackendController {

    private LatestAppInform informList;

    public InformController(String host) {
        super(host);
    }

    public LatestAppInform loadLatestInform() {
        long latest  = 0;
        if (informList != null) {
            latest = informList.getLatest();
        } else {
            String cache = getBackendCacheText("latest_inform");
            if (cache != null) {
                latest = Long.valueOf(cache);
            }
        }

        informList = BackendInform.getLatestInform(host, latest);
        if (informList != null) {
            setBackendCache("latest_inform", String.valueOf(informList.getLatest()));
        }
        return informList;
    }
}
