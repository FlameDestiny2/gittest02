package com.qb.xrealsys.ifafu.db;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Setting extends RealmObject {

    @PrimaryKey
    private String name;

    private String cell;

    public Setting() {
    }

    public Setting(String name, String cell) {
        this.name = name;
        this.cell = cell;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public static void update(final String name, final String value) {
        Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(new Setting(name, value));
            }
        });
    }

    public static String read(String name) {
        Setting setting = Realm.getDefaultInstance().where(Setting.class).equalTo("name", name).findFirst();
        if (setting == null) {
            return null;
        }

        return setting.getCell();
    }
}
