package com.qb.xrealsys.ifafu.backend.model;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

public class AppVersion extends BackendResponse {

    private String  deviceType;

    private int     versionCode;

    private String  versionName;

    private boolean forceUpdating;

    private String  comment;

    private String  downloadUrl;

    public AppVersion(@NotNull JSONObject object) {
        super(object);

        if (getStatus() == 0) {
            setComplete(false);

            try {
                JSONObject version = object.getJSONObject("version");
                setDeviceType(version.getString("deviceType"));
                setVersionCode(version.getInt("versionCode"));
                setVersionName(version.getString("versionName"));
                setForceUpdating(version.getBoolean("forceUpdating"));
                setComment(version.getString("comment"));
                setDownloadUrl(version.getString("downloadUrl"));
                setComplete(true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public boolean isForceUpdating() {
        return forceUpdating;
    }

    public void setForceUpdating(boolean forceUpdating) {
        this.forceUpdating = forceUpdating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
}
