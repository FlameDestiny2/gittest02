package com.qb.xrealsys.ifafu.tool;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.support.v4.content.PermissionChecker;
import android.telephony.TelephonyManager;

import java.util.Locale;
import java.util.UUID;

public class ReadPhone {

    public static String readIMEI(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (PermissionChecker.checkPermission(
                    context,
                    Manifest.permission.READ_PHONE_STATE,
                    Binder.getCallingPid(),
                    Binder.getCallingUid(),
                    context.getPackageName()) == PackageManager.PERMISSION_GRANTED) {
                return telephonyManager.getDeviceId();
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String readUUID(Context context) {
        String deviceId = readIMEI(context);
        if (deviceId.isEmpty()) {
            deviceId = UUID.randomUUID().toString();
        }

        return deviceId;
    }


    public static String readMac(Context context) {
        try {
            WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo    info = wifi.getConnectionInfo();
            return info.getMacAddress();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String readDeviceType() {
        return String.format(Locale.getDefault(), "%s-%s", Build.MANUFACTURER, Build.MODEL);
    }

    private static String readAndroidVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String readDeviceContent() {
        return String.format(Locale.getDefault(), "%s,%s", readDeviceType(), readAndroidVersion());
    }
}
