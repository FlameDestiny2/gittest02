package com.qb.xrealsys.ifafu.backend.model;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

public class PublicText extends BackendResponse {

    private String content;

    private int    t;

    public PublicText(@NotNull JSONObject object) {
        super(object);

        if (getStatus() == 0) {
            setComplete(false);
        }
        if (getStatus() == 0 && object.has("content") && object.has("t")) {
            try {
                setContent(object.getString("content"));
                setT(object.getInt("t"));
                setComplete(true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }
}
