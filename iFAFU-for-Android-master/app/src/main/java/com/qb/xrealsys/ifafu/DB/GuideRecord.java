package com.qb.xrealsys.ifafu.db;

import io.realm.RealmObject;

public class GuideRecord extends RealmObject {

    private String guideName;

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }
}
