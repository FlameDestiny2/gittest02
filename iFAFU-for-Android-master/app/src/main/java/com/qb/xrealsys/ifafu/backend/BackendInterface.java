package com.qb.xrealsys.ifafu.backend;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.qb.xrealsys.ifafu.backend.controller.InformController;
import com.qb.xrealsys.ifafu.backend.controller.ReportController;
import com.qb.xrealsys.ifafu.backend.controller.ResourceController;
import com.qb.xrealsys.ifafu.backend.controller.VersionController;
import com.qb.xrealsys.ifafu.backend.delegate.BackendCallback;
import com.qb.xrealsys.ifafu.backend.delegate.ReadBackendInformCallback;
import com.qb.xrealsys.ifafu.backend.model.AppVersion;
import com.qb.xrealsys.ifafu.db.Setting;
import com.qb.xrealsys.ifafu.tool.ConfigHelper;

import java.util.concurrent.ExecutorService;

public class BackendInterface {

    protected ExecutorService   threadPool;

    protected String            host;

    private BackendCallback     callback;

    private ResourceController  resourceController;

    private VersionController   versionController;

    private ReportController    reportController;

    private InformController    informController;

    private ConfigHelper        configHelper;

    private ReadBackendInformCallback informCallback;

    public BackendInterface(ExecutorService threadPool, ConfigHelper configHelper) {
        this.threadPool     = threadPool;
        this.configHelper   = configHelper;
        this.host           = configHelper.getSystemValue("backend");

        this.resourceController = new ResourceController(this.host);
        this.versionController  = new VersionController(this.host);
        this.reportController   = new ReportController(this.host);
        this.informController   = new InformController(this.host);
    }

    public void reportLogin(final String stuNo, final String stuClass, final String stuCollege, final String stuMajor) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                Log.i("BACKEND", "REPORT LOGIN: " + stuNo);
                reportController.reportUser(Setting.read("deviceId"), stuNo, stuClass, stuCollege, stuMajor);
            }
        });
    }

    public void reportState(final String option) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                Log.i("BACKEND", "REPORT STATE: " + option);
                reportController.reportState(Setting.read("deviceId"), option);
            }
        });
    }

    public void registerDevice(final Context context) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                callback.registerDeviceFinished(reportController.register(context));
            }
        });
    }

    public void setCallback(BackendCallback callback) {
        this.callback = callback;
    }

    public void loadResource() {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                resourceController.downloadBackground();
                resourceController.loadFirstWeek();
                resourceController.loadScoreCalcRule();
                versionController.loadAppLatestVersion();

                if (callback != null) {
                    callback.loadFinished();
                }
            }
        });
    }

    public void readAppInform() {
        /*
            有罪判决

            你将会判我多重的罪行来承担我过错？

            终身监禁早愿伸手被俘获

            无期徒刑只为能守护在你左右
        */
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                if (informCallback != null) {
                    informCallback.readBackendInform(informController.loadLatestInform());
                }
            }
        });
    }

    public Bitmap getBackground() {
        return resourceController.getBackground();
    }

    public String getFirstWeek() {
        return resourceController.getFirstWeek();
    }

    public String getScoreCalcRule() {
        return resourceController.getScoreCalcRule();
    }

    public AppVersion getLatestVersion() {
        return versionController.getAppVersion();
    }

    public void setInformCallback(ReadBackendInformCallback informCallback) {
        this.informCallback = informCallback;
    }
}
