package com.qb.xrealsys.ifafu.main.layout;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.qb.xrealsys.ifafu.R;
import com.qb.xrealsys.ifafu.tool.GlobalLib;

import java.util.Locale;

import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrUIHandler;
import in.srain.cube.views.ptr.indicator.PtrIndicator;

public class PtrCustomHeader extends FrameLayout implements PtrUIHandler {

    private TextView    mTitleTextView;

    private TextView    mHistoryView;

    private Activity    mActivity;

    private ImageView   mReloadIcon;

    private ProgressBar mReloadingIcon;

    public PtrCustomHeader(Context context) {
        super(context);
        initView(context);
    }

    public PtrCustomHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public PtrCustomHeader(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    //初始化自定义布局文件
    private void initView(Context context) {
        //这里加载自定义的布局文件
        inflate(context, R.layout.gadget_reload, this);
        //找到布局内部的控件
        mActivity      = (Activity) context;
        mTitleTextView = findViewById(R.id.reloadTitle);
        mHistoryView   = findViewById(R.id.reloadHistory);
        mReloadIcon    = findViewById(R.id.reloadIcon);
        mReloadingIcon = findViewById(R.id.reloadingIcon);
        updateHistory(GlobalLib.getTimeString());
    }

    private void updateHistory(String time) {
        mHistoryView.setText(String.format(
                Locale.getDefault(),
                mActivity.getString(R.string.display_reload_history),
                time));
    }

    public void pauseAnim(){
        ObjectAnimator anim = ObjectAnimator.ofFloat(mReloadIcon, "rotation", 0f, 180f);
        anim.setDuration(500);
        anim.start();
    }

    public void passAnim(){
        ObjectAnimator anim = ObjectAnimator.ofFloat(mReloadIcon, "rotation", 180f, 0f);
        anim.setDuration(500);
        anim.start();
    }

    //初始化状态
    @Override
    public void onUIReset(PtrFrameLayout frame) {
        //这个方法可以不用管   也可以在这里关闭动画
    }

    //开始向下拉的时候调用
    @Override
    public void onUIRefreshPrepare(PtrFrameLayout frame) {
    }

    //刷新过程时调用
    @Override
    public void onUIRefreshBegin(PtrFrameLayout frame) {
        mReloadIcon.setVisibility(INVISIBLE);
        mReloadingIcon.setVisibility(VISIBLE);
    }

    //刷新完成后调用,向上移动时调用
    @Override
    public void onUIRefreshComplete(PtrFrameLayout frame) {
        //可以不断的改变动画效果以及切换显示的控件
        mTitleTextView.setText(R.string.display_reload_default);
        mReloadIcon.setVisibility(VISIBLE);
        mReloadingIcon.setVisibility(INVISIBLE);
        updateHistory(GlobalLib.getTimeString());
        passAnim();
//        animationDrawable.stop();  //模拟动画
//        animationDrawable.start();
    }

    //重复下拉
    @Override
    public void onUIPositionChange(PtrFrameLayout frame, boolean isUnderTouch, byte status, PtrIndicator ptrIndicator) {
        //在同一次下拉中不断向上向下移动,这里可以不断改变显示效果
        final int mOffsetToRefresh = frame.getOffsetToRefresh();
        final int currentPos = ptrIndicator.getCurrentPosY();  //获取到下拉的高度
        final int lastPos = ptrIndicator.getLastPosY();      //最大下拉的高度
        //根据下拉的位置进行控件的显示
        if (currentPos < mOffsetToRefresh && lastPos >= mOffsetToRefresh) {
            if (isUnderTouch && status == PtrFrameLayout.PTR_STATUS_PREPARE) {
                crossRotateLineFromBottomUnderTouch(frame); //调用方法
            }
        } else if (currentPos > mOffsetToRefresh && lastPos <= mOffsetToRefresh) {
            if (isUnderTouch && status == PtrFrameLayout.PTR_STATUS_PREPARE) {
                crossRotateLineFromTopUnderTouch(frame);  //调用方法
            }
        }
    }
    //下拉到可以刷新时显示
    private void crossRotateLineFromTopUnderTouch(PtrFrameLayout frame) {
        if (!frame.isPullToRefresh()) {
            pauseAnim();
            mTitleTextView.setText(R.string.display_reload_pause);
        }
    }
    //动态改变文字
    private void crossRotateLineFromBottomUnderTouch(PtrFrameLayout frame) {
        mTitleTextView.setText(R.string.display_reload_default);
    }
}